<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Models\ana;
use Illuminate\Http\Request;

class AnaController extends BaseController
{
   public function ana(){ 
   
		$sonuclar = ana::all();
		return view('ana', array('sonuclar' => $sonuclar));
   }

   public function sil($id){ 
   
		ana::where('id','=',$id)->delete();
		return redirect('/');
   }



   public function ekle(Request $request){ 
   
		$form = $request->all();
		
		$o = new ana;
		$o->adi = $form['adi'];
		$o->soyadi = $form['soyadi'];
		$o->save();
		return redirect('/');
   }
   public function sakla(Request $request){ 
   
		$form = $request->all();
		
		$o = new ana;
		$o->adi = $form['adi'];
		$o->soyadi = $form['soyadi'];
		$o->save();
		return redirect('/');
   }
   
}
