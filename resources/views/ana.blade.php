<!DOCTYPE html>
<html>
    <head>
        <title>Laravel</title>

        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">

        <style>
            html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                display: table;
                font-weight: 100;
                font-family: 'Lato';
            }

            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }

            .content {
                text-align: center;
                display: inline-block;
            }

            .title {
                font-size: 96px;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="content">
                <div class="title">Laravel 5</div>
				
				<a href='./yeni'>Yeni</a>
				
				<table>
				<tr><td>No</td> <td>Ad</td> <td>Soyad</td> </tr>
				@foreach( $sonuclar as $s)
				<tr><td>{{$s->id}}</td> <td>{{$s->adi}}</td> <td>{{$s->soyadi}}</td> <td> <a href="guncelle/{{$s->id}}">G�ncelle</a></td> <td> <a href="sil/{{$s->id}}">Sil</a></td>  </tr>
				@endforeach
				</table>
            </div>
        </div>
    </body>
</html>
